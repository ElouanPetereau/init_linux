<!-- omit in toc -->
# A repo to setup a linux session according to my taste

- [Initialisation script](#initialisation-script)
    - [List of distribution independant modifications](#list-of-distribution-independant-modifications)
      - [Added folder](#added-folder)
      - [Added scripts to user home](#added-scripts-to-user-home)
      - [Added configuration](#added-configuration)
    - [List of debian based distribution modifications](#list-of-debian-based-distribution-modifications)
      - [Added configuration](#added-configuration-1)
    - [List of nixos based distribution modifications](#list-of-nixos-based-distribution-modifications)
      - [Added configuration](#added-configuration-2)
    - [List of extensions](#list-of-extensions)

  
---

 ### Initialisation script
Use the [`initlinux`](./initlinux) bash script to setup the working environment.    
This script will try to detect automatically the used distribution, if it fails to do so, use the `-d <distribution>` option. 

##### List of distribution independant modifications
###### Added folder
- Workspace to ```$HOME/Workspace```
- bin to ```$HOME/bin```

###### Added scripts to user home
- `initbs`: A bash script to initialize bash scripts
- `backfiles`: A bash script to backup files
- `compare`: A bash script to compare files
- `vimplugin_install`: A bash script to easily install vim plugins
- `docker-xbuild `: A bash script to build docker images 
- `docker-rmi`: A bash script to remove docker images

###### Added configuration
- vscode and vscodium `settings.json` and `keybindings.json`

##### List of debian based distribution modifications
###### Added configuration
- `.vimrc`: Vim configuration file
- `.profile`: Profile configuration file
- `.bashrc`: bash configuration file
- `.bash_aliase`: Bash aliases configuration file
- `.gitconfig`: Git configuration file
- `powerline`:Shell interpreter configuration and themes
- `terminator`: Terminal application configuration (see [setup_powerline.md](./docs/setup_powerline.md) for installation guide)


##### List of nixos based distribution modifications
###### Added configuration
- `configuration.nix`: For the main nix configuration (see [setup_nix.md)](./docs/setup_nix.md) for more information)
- `home.nix`: For the main `home-manager` configuration
- `packages.nix`: For the nix packages to install using `home-manager`
- `gnome.nix`: For setting the gnome desktop environment using `home-manager` settings
- `bash.nix`: For setting the bash configuration using `home-manager`
- `vim.nix`: For the the vim configuration using `home-manager`
- `git.nix`: For the the git configuration using `home-manager`

##### List of extensions
Use the  `-e` option of `initlinux` to install extensions for the given application.    
The list of available extension is listed in [extension_list.md](./docs/extension_list.md).
