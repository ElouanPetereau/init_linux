# Setup powerline

See [official guide](https://powerline.readthedocs.io/en/master/installation/linux.html) for more details.

### Powerline installation
```
sudo apt install powerline
sudo apt install powerline-gitstatus
```

### Fonts installation

```
sudo apt-get install fonts-powerline
```