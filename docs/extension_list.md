<!-- omit in toc -->
# List of application extensions

> Note that this list might be outdated, check the [extension_list](../extensions_list/) folder for an up to date list.

- [vscode/vscodium](#vscodevscodium)
- [vim](#vim)

---

#### vscode/vscodium
- [gitlens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens): Better git support
- [git graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph): Add a git tree like gitk
<p></p>

- [docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker): Docker support
- [remote containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers): Work inside vscode through a docker container
- [remote ssh](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh): Work inside vscode through ssh
<p></p>

- [markdown preview enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced): Preview markdown files
- [markdown pdf](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf): Convert markdown file into a pdf
- [markdown all in one](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one): Markdown syntax support
- [plantUML](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml): Create class diagrams
- [Tex workshop](https://marketplace.visualstudio.com/items?itemName=Jeff-Tian.tex-workshop): LaTex support
- [vscode pdf](https://marketplace.visualstudio.com/items?itemName=tomoki1207.pdf): pdf support
<p></p>

- [rainbow brackets](https://marketplace.visualstudio.com/items?itemName=2gua.rainbow-brackets): Colorize matching brackets
- [peacock](https://marketplace.visualstudio.com/items?itemName=johnpapa.vscode-peacock): Colorize each workspaces differently
- [material icon theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme): Custom icons
- [todo highlight](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight): See TODO et FIXME tags in a project
- [hex hover converter](https://marketplace.visualstudio.com/items?itemName=maziac.hex-hover-converter): Convert binary numbers while hovering
- [regex previewer](https://marketplace.visualstudio.com/items?itemName=chrmarti.regex): Regex match previewer
- [code spell checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker): English spelling checker
- [french code spell checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-french): French spelling checker
<p></p>

- [cpptools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools): C/C++ extended support
- [cmake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake): Cmake syntax support
- [cmake tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools): Cmake support
<p></p>

- [python](https://marketplace.visualstudio.com/items?itemName=ms-python.python): Python support
- [jupyter](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter): Jupyter notebooks support
- [jupyter keymap](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter-keymap): Jupyter keymaps for notebooks
- [jupyter notebook renderers](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter-renderers): Renderers for jupyter notebook (plots, images...)
<p></p>

- [shell format](https://marketplace.visualstudio.com/items?itemName=foxundermoon.shell-format): Shell formatter
- [shell check](https://marketplace.visualstudio.com/items?itemName=timonwong.shellcheck): Shell support
<p></p>

- [rainbow csv](https://marketplace.visualstudio.com/items?itemName=mechatroner.rainbow-csv): Better csv file visualisation + allow sql-like query
- [sql formatter](https://marketplace.visualstudio.com/items?itemName=adpyke.vscode-sql-formatter): SQL formatter


#### vim
- [Vim-easymotion](https://github.com/easymotion/vim-easymotion): Use `<Leader><Leader>w` to move fast to a word 	
- [NerdTree](https://github.com/preservim/nerdtree): Use `Ctrl+N` to open file explorer
- [NerdCommenter](https://github.com/preservim/nerdcommenter) : Use `<Leader>cc` to comment a line and `<Leader>cu` to uncomment
- [Ale](https://github.com/dense-analysis/ale) : A vim linter 
    >use gcc for c/c++ linting (apt-get install build-essential)
 	>use shellcheck for bash linting (apt-get install shellcheck)
- [GitGutter](https://github.com/airblade/vim-gitgutter) : Shows a git diff in the sign column
- [Taglist](https://github.com/vim-scripts/taglist.vim) : `Ctrl+t` to browse through code 
- [Powerline](https://github.com/powerline/powerline): Support powerline status bar (powerline need to be installed separatly (see []()))
