# Update NixOs

How to update nix and the packages

## Check and set the latest channel

Compare if the current channel is the lates (see [Nix Channel Status](https://status.nixos.org/)):
```bash
sudo nix-channel --list
```

If not on the latest, upgrade it:
```bash
sudo nix-channel --remove nixos
sudo nix-channel --add https://nixos.org/channels/nixos-21.11 nixos
```

| :warning:  Warning |
|:----------------------------------------------|
| Do not forget to update `home.stateVersion` in `./config/home-manager/home.nix` to the new nix version.|


## Update the channel

```bash
sudo nix-channel --update
```

## Update the packages 

Update all packages declared in NixOS `configuration.nix`:

```bash
sudo nixos-rebuild switch
```

Update all packages installed with nix-env:

```bash
nix-env -u '*'
```

Update all packages installed using Home Manager:

```bash
home-manager switch
```
