# Setting up NixOs

Here are my working notes on getting a NixOS system up and running.

> **/!\\ WARNING /!\\**
You can run into a hidden problem that will prevent a correct partition setup and `/etc/nixos/configuration.nix` from working.
If you are setting up a UEFI system, then you need to make sure you boot into the NixOS installation from the UEFI partition of the bootable media. You may have to enter your BIOS boot selection menu to verify this.
For example, if you setup a NixOS installer image on a flash drive, your BIOS menu may display several boot options from that flash drive: choose the one explicitly labeled with "UEFI".

- [Setting up NixOs](#setting-up-nixos)
  - [References](#references)
  - [Setup Nix with LUKS encrypted root](#setup-nix-with-luks-encrypted-root)
      - [Wipe existing fs](#wipe-existing-fs)
      - [Partition disk](#partition-disk)
      - [Encrypt Primary Disk](#encrypt-primary-disk)
      - [Format Disks](#format-disks)
      - [Mount](#mount)
      - [Resulting Disk](#resulting-disk)
      - [Nix configuration](#nix-configuration)
  - [Install NixOs](#install-nixos)
  - [Setup configuration](#setup-configuration)
      - [Add gnome and generic packages](#add-gnome-and-generic-packages)
      - [Install user packages](#install-user-packages)


---

## References

I used these resources:

- Nixos manual: https://nixos.org/nixos/manual/index.html#sec-installation
- Nixos wiki on full disk encryption: https://nixos.wiki/wiki/Full_Disk_Encryption
- walkermalling's gist: https://gist.github.com/walkermalling/23cf138432aee9d36cf59ff5b63a2a58

---

## Setup Nix with LUKS encrypted root

Start by taking a look at block devices and identify the name of the device you're setting up. Note that adding the `--fs` flag will show the UUID of each device.

```bash
lsblk
```
#### Wipe existing fs
I generaly boot on a live iso with `gparted` to wipe the full disk and create a new GPT partition table but the following can also be used:

```bash
sudo wipefs -a /dev/sda
```
> Note that Cryptsetup FAQ suggests we use `cat /dev/zero > [device target]`


#### Partition disk

Create a new partition table:
```bash
sudo parted /dev/sda -- mklabel gpt
```

Create the boot partition at the beginning of the disk:
```bash
sudo parted /dev/sda -- mkpart ESP fat32 1MiB 512MiB
sudo parted /dev/sda -- set 1 boot on
```

Create primary partition:
```bash
sudo parted /dev/sda -- mkpart primary 512MiB 100%
```

Now `/dev/sda1` is our boot partition, and `/dev/sda2` is our primary.

#### Encrypt Primary Disk

Setup luks on sda2 (`encrypted` is the label). This will prompt for creating a password.
```bash
sudo cryptsetup luksFormat /dev/sda2
sudo cryptsetup luksOpen /dev/sda2 encrypted
```

Map the physical, encrypted volume, then create a new volume group and logical volumes in that group for our nixos root and our swap:
```bash
sudo pvcreate /dev/mapper/encrypted
sudo vgcreate vg /dev/mapper/encrypted
sudo vgdisplay
sudo lvcreate -L 8G -n swap vg
sudo lvcreate -l '100%FREE' -n nixos vg
sudo lvdisplay
```

#### Format Disks

The boot volume will be fat32 and the filesystem will be ext4. Also creating a swap:
```bash
sudo mkfs.fat -F 32 -n boot /dev/sda1
sudo mkfs.ext4 -L nixos /dev/vg/nixos
sudo mkswap -L swap /dev/vg/swap
```


#### Mount

Mount the target file system to `/mnt`:
```bash
sudo mount /dev/disk/by-label/nixos /mnt
```

Mount the boot file system on `/mnt/boot` for UEFI boot:
```bash
sudo mkdir -p /mnt/boot
sudo mount /dev/disk/by-label/boot /mnt/boot
```

Activate swap:
```bash
sudo swapon /dev/vg/swap
```

#### Resulting Disk

Expect the following result:

```
NAME           MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
loop0            7:0    0   1.1G  1 loop  /nix/.ro-store
sda              8:0    0 232.9G  0 disk
├─sda1           8:1    0   511M  0 part  /mnt/boot
└─sda2           8:2    0 232.4G  0 part
  └─encrypted    254:0  0 232.4G  0 crypt
    ├─vg-swap    254:1  0     8G  0 lvm   [SWAP]
    └─vg-nixos   254:2  0 224.4G  0 lvm   /mnt
```

#### Nix configuration

https://nixos.org/manual/nixos/stable/#sec-installation-manual-installing

generate configuration:
```bash
sudo nixos-generate-config --root /mnt
```


Edit configuration at `/mnt/etc/nixos/configuration.nix`:
```nix
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader
  boot.loader.systemd-boot.enable = false;
  boot.loader.efi.canTouchEfiVariables = true;

  # grub
  boot.loader.grub = {
    enable = true;
    efiSupport = true;
    enableCryptodisk = true;
    device = "nodev";
  };
 
  # luks
  boot.initrd.luks.devices = {
    encrypted = {
      device = "dev/disk/by-uuid/7e91847b-5ff0-4e0f-aaed-e0d2c2d3a610";
      preLVM = true;
    };
  };

  # Hostname.
  networking.hostName = "TOASTER"; 
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Internationalisation properties and time zone.
  time.timeZone = "Europe/Paris";
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    keyMap = "fr";
  };

  # Setup user
  users.users.elouan = {
    isNormalUser  = true;
    home  = "/home/elouan";
    extraGroups  = [ "wheel" "networkmanager" ];
  };
}
```

The disk UUID can be obtained with the following command line: `ls -l /dev/disk/by-uuid/`.

Note the line `boot.loader.grub.device = "nodev";` this is a special value: 

> [nodev](https://nixos.org/nixos/manual/options.html#opt-boot.loader.grub.device): The device on which the GRUB boot loader will be installed. The special value nodev means that a GRUB boot menu will be generated, but GRUB itself will not actually be installed."

Note the absence of `boot.loader.efi.efiSysMountPoint = "/boot/efi"`. My installation would not succeed if I specified this.

Note that the name of the encrypted filesystem in `boot.initrd.luks.devices` is the name used in `cryptsetup luksOpen` and in `vgcreate`.


## Install NixOs

Run the installer:
```bash
sudo nixos-install
```

If install is successful, you'll be prompted to set password for root user. Then `reboot`, and remove installation media.

Login to root, and add add user:
```bash
sudo useradd -m elouan
sudo passwd elouan
sudo usermod -aG sudo elouan
```

## Setup configuration


#### Add gnome and generic packages
This will install and setup gnome, gnome-tweaks and home-manager. 

Simply copy the [configuration file](./conf_files/nixos/configuration.nix) into `/etc/nixos/configuration.nix`:
```bash
sudo cp ./conf_files/nixos/configuration.nix /etc/nixos/configuration.nix
```

The configuration file can also be manually changed at `/etc/nixos/configuration.nix`:
```nix
# Enable the X11 windowing system.
services.xserver.enable = true;
# Configure keymap in X11.
services.xserver.layout = "us,fr";

# Enable the GNOME Desktop environment.
services.xserver.displayManager.gdm.enable = true;
services.xserver.desktopManager.gnome.enable = true;

# Exclude all useless gnome files from the config
environment.gnome.excludePackages = (with pkgs; [
  gnome-photos
  gnome-tour
  gnome-connections
  ]) ++ (with pkgs.gnome; [
  cheese # webcam tool
  gnome-music
  gedit # text editor
  epiphany # web browser
  geary # email
  totem
  gnome-maps
  gnome-weather
  gnome-contacts
  tali # poker game
  iagno # go game
  hitori # sudoku game
  atomix # puzzle game
]);

# Allow to install non-free applications.
nixpkgs.config = {
  allowUnfree = true;
};

# Add system wide packages.
environment.systemPackages = [
  # git
  pkgs.git
  # gnome tweaks
  pkgs.gnome.gnome-tweaks
  # home manager
  pkgs.home-manager
];

# Setup bash shell.
users.defaultUserShell = pkgs.bash;
programs.bash.promptInit = ''PS1="[\[\e[31m\]\u\[\e[m\]\[\e[31m\]@\[\e[m\]\[\e[31m\]\h\[\e[m\]]:\[\e[33m\]\w\[\e[m\]#"'';
programs.bash.enableLsColors = true;
environment.interactiveShellInit = ''
  alias ls='eza'
  alias less='spin'
  alias cat='bat'
  alias top='btm'
'';

# Exclude Xterm
services.xserver.excludePackages = [ pkgs.xterm ];
```

Then build the new configuration, make it the default configuration for booting, and try to realise the configuration in the running system:
```bash
sudo nix-channel --update # Not sure if necessay
sudo nixos-rebuild switch --upgrade
```


#### Install user packages
This requires `home-manager` to be intialized.    
Use the `./initlinux` script to copy the required `home-manager` config files in `~/.config/home-manager/`.

```bash
./initlinux
home-manager switch
```
