#!/usr/bin/env bash

# Author : Elouan PETEREAU
# Date : mer. 13 mai 2020 11:12:31 CEST

#####################################
# GLOBAL VARIABLES
#####################################

# Colors list
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'

# Text styles
bld=$'\e[1m'
fnt=$'\e[2m'
itc=$'\e[3m'
udl=$'\e[4m'

# Reset all styles
end=$'\e[0m'

#####################################
# FUNCTIONS DEFINTIION
#####################################

usage() {
	echo "docker-xbuild (Docker extended build)"
	echo -e "A bash script made to build one or multiple docker images, auto increment it's build version and tagging it as latest\n"
	echo "Usage:"
	echo " docker-xbuild <docker_image_name>"
	echo " docker-xbuild [options] <docker_images_names>"
	echo -e "\nOptions:"
	echo " -h			Display script help informations"
	echo " -m			Increment the first number to make a major update (v1.0 -> v2.0)"
	echo " -t <tag_name>		Set the tag name instead of using the build version number"
	echo " -o			Only tag the image as the given tag or the version number not as latest"
	echo " -l <location>		Change Dockerfile location (default .)"
}

increment_last_number() {
	image_new_last_number=$(($(docker images | grep "$docker_image" | awk -F" " '{print $2}' | grep 'v[0-9]*\.[0-9]*' | sort -r | head -n 1 | sed -n "s|v\([0-9]*\)\.\([0-9]*\)|\2| p") + 1))
	image_new_version_number=$(docker images | grep "$docker_image" | awk -F" " '{print $2}' | grep 'v[0-9]*\.[0-9]*' | sort -r | head -n 1 | sed -n "s|v\([0-9]*\)\.\([0-9]*\)|\1\.$image_new_last_number| p")
}

increment_first_number() {
	image_new_first_number=$(($(docker images | grep "$docker_image" | awk -F" " '{print $2}' | grep 'v[0-9]*\.[0-9]*' | sort -r | head -n 1 | sed -n "s|v\([0-9]*\)\.\([0-9]*\)|\1| p") + 1))
	image_new_version_number=$(docker images | grep "$docker_image" | awk -F" " '{print $2}' | grep 'v[0-9]*\.[0-9]*' | sort -r | head -n 1 | sed -n "s|v\([0-9]*\)\.\([0-9]*\)|$image_new_first_number\.0| p")
}

build_image() {
	if [ "$tag_latest" = true ]; then
		docker build -t "$docker_image" -t "$docker_image":"$tag_name" "$dockerfile"
	else
		docker build -t "$docker_image":"$tag_name" "$dockerfile"
	fi
}

#####################################
# INITIALISATION
#####################################

unset options

docker_image=""
docker_images=""
dockerfile="."
is_new_main_version=false
image_new_last_number=""
image_new_first_number=""
image_new_version_number=""

tag_name=""
tag_latest=true
tag_using_version=true
#####################################
# MAIN
#####################################

# Get the options if there is some
while getopts ":hml:ot:" option; do
	case $option in
	"h")
		usage
		exit 0
		;;
	"m")
		is_new_main_version=true
		;;
	"l")
		dockerfile=$OPTARG
		;;
	"o")
		tag_latest=false
		;;
	"t")
		tag_using_version=false
		tag_name=$OPTARG
		;;
	?)
		echo >&2 "unknown option"
		exit 1
		;;
	*)
		echo >&2 "unknown option"
		;;
	esac
done
shift $((OPTIND - 1))

docker_images=("$@")

[ "${#docker_images[@]}" -eq 0 ] && printf >&2 '%s\n' "${red}Error : script need at least a docker image name, try 'docker-xbuild -h' for more information.${end}"

for docker_image in "${docker_images[@]}"; do

	if [ "$tag_using_version" = true ]; then
		if [ "$is_new_main_version" = true ]; then
			increment_first_number
		else
			increment_last_number
		fi
		[ -z "$image_new_version_number" ] && image_new_version_number="1.0"
		tag_name="v${image_new_version_number}"
	fi

	build_image

done
