# Aliases for coreutils apps
alias grep="grep --color=auto"
alias fgre=p"fgrep --color=auto"
alias egrep="egrep --color=auto"
if command -v bat &>/dev/null; then
    alias cat="bat"
fi
if command -v tspin &>/dev/null; then
    alias less="tspin"
fi
if command -v eza &>/dev/null; then
    alias ls="eza --color=auto"
    alias ll="ls -alhgF --git"
    alias la="ls -ah"
    alias ld="ls -alhgFr"
    alias lf="ls -alhgFT"
else
    alias ls='ls --color=auto'
    alias ll='ls -AlhF'
    alias la='ls -Ah'
    alias ld='ls -AlhFtr'
fi

# Add an "alert" alias for long running commands.  Use like so:
# sleep 10; alert
alias alert="notify-send --urgency=normal \"command finished with $(if [ $? = 0 ]; then echo \"success\"; else echo \"error\"; fi)\" \"Return code is: $?\""

# Directory aliases
alias cdgit="cd $HOME/Workspaces/Git_Workspace"
alias cdwp="cd $HOME/Workspaces"

alias dudh="du -h -d 1"

# Apt aliases
alias sudupd="sudo apt-get update"
alias sudupg="sudo apt-get upgrade"
alias sudfup="sudo -- sh -c 'apt-get update; apt-get upgrade -y; apt-get autoremove -y; apt-get autoclean -y'"

# Disable webcam until next reboot alias
alias disablecam="sudo modprobe -r uvcvideo"

#Disable/enable trackpad
alias trackon="synclient TouchpadOff=0"
alias trackoff="synclient TouchpadOff=1"

# Archives aliases
alias targz="tar -cvzf"
alias untargz="tar -xvzf"
alias tarbz2="tar -cvjf"
alias untarbz2="tar -xvjf"

# Ps aliases
alias psgrep="ps -aefx -o user,pid,group,gid,comm|head -n 1 ; ps -aefx -o user,pid,group,gid,comm|egrep"

# Shutdown alias
alias shutnow='shutdown -h now'
