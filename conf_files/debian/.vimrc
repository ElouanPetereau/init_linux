" Set default color syntax
syntax on
filetype plugin on
colorscheme desert  

" Set <leader> to ù
let mapleader = "ù"

" Set vim updatetime to 100ms
set updatetime=100

" Set show line number by default
set number

" Better command-line completion
set wildmenu

" Show partial commands in the last line of the screen
set showcmd

" Highlight searches (use <C-L> to temporarily turn off highlighting)
set hlsearch

" Keep the same indent as the line you're currently on
set autoindent

" Map <C-L> (redraw screen) to also turn off search highlighting until the next search
nnoremap <C-L> :nohl<CR><C-L>

" INSTALLED VIM PLUGINS LIST (:scriptnames to show loaded vim plugins) :
" - Vim-easymotion : <Leader><Leader>w to move fast to a word
" 	(https://github.com/easymotion/vim-easymotion)
" - Powerline : better status bar
"   	(https://github.com/powerline/powerline) 
" - NerdTree : Ctrl+N to open file explorer
"	(https://github.com/preservim/nerdtree)
" - NerdCommenter :  <Leader>cc comment | <Leader>cu uncomment
"   	(https://github.com/preservim/nerdcommenter)
" - Ale : vim linter 
"   	use gcc for c/c++ linting (apt-get install build-essential)
"   	use shellcheck for bash linting (apt-get install shellcheck)
"   	(https://github.com/dense-analysis/ale)
" - GitGutter : shows a git diff in the sign column
"   	(https://github.com/airblade/vim-gitgutter)
" - Taglist : Ctrl+t to browse through code 
"   	(https://github.com/vim-scripts/taglist.vim)

" Setup powerline plugin (https://github.com/powerline/powerline)
python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup
set t_Co=256
set noshowmode
set laststatus=2

" Setup NERDTree plugin (https://github.com/preservim/nerdtree)
" Close automatically if the only window left open is the NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Map a specific shortcut to open NERDTree
map <C-n> :NERDTreeToggle<CR>
" Show hidden files by default
let NERDTreeShowHidden=1

" Setup NERDCommenter plugin (https://github.com/preservim/nerdcommenter)
" Add a space after comment character
let g:NERDSpaceDelims = 1
" Comment empty lines 
let g:NERDCommentEmptyLines = 1
" Remove spaces when disabling comments
let g:NERDTrimTrailingWhitespace = 1
" Set comment to be at the start of the line
let g:NERDDefaultAlign = 'left'

" Setup taglist plugin (https://github.com/vim-scripts/taglist.vim)
" Map a specific shortcut to open the taglist 
nnoremap <C-t> :TlistToggle<CR>
" Close automatically if the only window left open is the taglist 
let Tlist_Exit_OnlyWindow = 1
" Go in the taglist windows on toggle
let Tlist_GainFocus_On_ToggleOpen = 1

