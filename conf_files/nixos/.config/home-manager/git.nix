{ pkgs, ... }: {
  programs.git = {
    enable = true;
    userName = "Elouan Petereau";
    userEmail = "elouan.p@gmail.com";
    aliases = {
      tree = "log --oneline --graph --color --all --decorate";
      subtree-list = "!git log | grep -A 1 git-subtree-dir | grep -v \"\\-\\-\" | tr -d \" \" | cut -d \":\" -f2 | xargs -I {} bash -c \"if [[ ! {} =~ [0-9a-f]{40} ]]; then if [[ -d $(git rev-parse --show-toplevel)/{} ]]; then echo \\\"found project : {}\\\"; else echo \\\"found project only in commit history : {}\\\"; fi; else echo -e \\\"\\tcommit : {}\\\";fi\"";
      submodule-list = "!find ./ -type f -name \".gitmodules\"|xargs cat";
      find-added = "log --diff-filter=A";
      find-copied = "log --diff-filter=C";
      find-deleted = "log --diff-filter=D";
      find-modified = "log --diff-filter=M";
      find-renamed = "log --diff-filter=R";
      find-changed = "log --diff-filter=T";
      find-unmergeded = "log --diff-filter=U";
      find-unkonwm = "log --diff-filter=X";
      find-broken = "log --diff-filter=B";
      kdiff3 = "difftool -d --tool=kdiff3";
      med = "difftool -d --tool=meld";
      pushf = "push --force-with-lease";
      prune-merged = "!git remote prune origin && git branch --merged | egrep -v \"(^\\*|master|main|dev)\" | xargs git branch -d";
    };
    lfs.enable = true;
    extraConfig = {
      core = { editor = "vi"; };
      diff = { tool = "meld"; };
      pager = {
        diff = true;
        jelp = true;
        log = true;
        tree = true;
        subtree-list = true;
      };
      color = { ui = "auto"; };
    };
  };
}
