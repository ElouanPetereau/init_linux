{ pkgs, ... }: {
  # Home mamanger config.
  home.stateVersion = "23.11";
  home.username = "elouan";
  home.homeDirectory = "/home/elouan/";

  # Allow unfree and insecure packages.
  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnfreePredicate = (_: true);
      permittedInsecurePackages = [
        "electron-19.1.9"
      ];
    };
  };

  # Allow home manager to manage itself.
  programs.home-manager.enable = true;

  # Import packages.
  imports = [
    ./packages.nix
    ./gnome.nix
    ./git.nix
    ./bash.nix
    ./vim.nix
  ];
}
