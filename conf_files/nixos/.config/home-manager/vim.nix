{ pkgs, ... }: {
  programs.vim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      vim-easymotion
      nerdtree
      nerdcommenter
      ale
      vim-gitgutter
      taglist-vim
    ];
    settings = { number = true; };
    extraConfig = ''
      " Set default color syntax
      syntax on
      filetype plugin on
      colorscheme desert  

      " Set <leader> to ù
      let mapleader = "ù"

      " Set vim updatetime to 100ms
      set updatetime=100

      " Better command-line completion
      set wildmenu

      " Show partial commands in the last line of the screen
      set showcmd

      " Highlight searches (use <C-L> to temporarily turn off highlighting)
      set hlsearch

      " Keep the same indent as the line you're currently on
      set autoindent

      " Map <C-L> (redraw screen) to also turn off search highlighting until the next search
      nnoremap <C-L> :nohl<CR><C-L>

      set t_Co=256
      set noshowmode
      set laststatus=2

      " Setup NERDTree plugin (https://github.com/preservim/nerdtree)
      " Close automatically if the only window left open is the NERDTree
      autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
      " Map a specific shortcut to open NERDTree
      map <C-n> :NERDTreeToggle<CR>
      " Show hidden files by default
      let NERDTreeShowHidden=1

      " Setup NERDCommenter plugin (https://github.com/preservim/nerdcommenter)
      " Add a space after comment character
      let g:NERDSpaceDelims = 1
      " Comment empty lines 
      let g:NERDCommentEmptyLines = 1
      " Remove spaces when disabling comments
      let g:NERDTrimTrailingWhitespace = 1
      " Set comment to be at the start of the line
      let g:NERDDefaultAlign = 'left'

      " Setup taglist plugin (https://github.com/vim-scripts/taglist.vim)
      " Map a specific shortcut to open the taglist 
      nnoremap <C-t> :TlistToggle<CR>
      " Close automatically if the only window left open is the taglist 
      let Tlist_Exit_OnlyWindow = 1
      " Go in the taglist windows on toggle
      let Tlist_GainFocus_On_ToggleOpen = 1
    '';
  };

}

