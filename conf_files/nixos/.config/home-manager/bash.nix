{ pkgs, ... }: {
  programs.bash = {
    enable = true;

    historySize = 1000;
    historyFileSize = 2000;

    initExtra = ''
      export PS1="[\[\033[01;32m\]\u@\h\[\033[00m\]]:\[\033[01;34m\]\w\[\033[00m\]\$"
      export PAGER=bat
      # Colored GCC warnings and errors
      export GCC_COLORS="error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01"
      export PATH=$PATH:"$HOME/bin"
    '';

    shellAliases = {
      # Add an "alert" alias for long running commands.  Use like so: 
      # sleep 10; alert 
      alert = "notify-send --urgency=normal \"command finished with $(if [ $? = 0 ]; then echo \"success\"; else echo \"error\"; fi)\" \"Return code is: $?\"";

      # ls aliases
      ll = "ls -alhgF --git";
      la = "ls -ah";
      ld = "ls -alhgFr";
      lf = "ls -alhgFT";

      # Directory aliases
      cdgit = "cd $HOME/Workspaces/Git_Workspace";
      cdwp = "cd $HOME/Workspaces";

      dudh = "du -h -d 1";

      # Disable webcam until next reboot alias
      disablecam = "sudo modprobe -r uvcvideo";

      #Disable/enable trackpad
      trackon = "synclient TouchpadOff=0";
      trackoff = "synclient TouchpadOff=1";

      # Archives aliases
      targz = "tar -cvzf";
      untargz = "tar -xvzf";
      tarbz2 = "tar -cvjf";
      untarbz2 = "tar -xvjf";

      # Ps aliases
      psgrep = "ps -aefx -o user,pid,group,gid,comm|head -n 1 ; ps -aefx -o user,pid,group,gid,comm|egrep";

      # Shutdown alias
      shutnow = "shutdown -h now";

      # Aliases for coreutils apps
      cat = "bat";
      less = "spin";
      ls = "eza --color=auto";
      grep = "grep --color=auto";
      fgrep = "fgrep --color=auto";
      egrep = "egrep --color=auto";
    };
  };
}
