# Obtained using dconf dump /
{ pkgs, ... }: {
  dconf.settings = {
    "org/gnome/Console" = {
      theme = "night";
    };

    "org/gnome/desktop/calendar" = {
      show-weekdate = false;
    };
    "org/gnome/desktop/calendar" = {
      clock-show-seconds = true;
      clock-show-weekday = true;
      enable-hot-corners = false;
      font-antialiasing = "grayscale";
      font-hinting = "slight";
    };
    "org/gnome/shell/world-clocks" = {
      locations = "@av []";
    };
    "org/gnome/desktop/interface" = {
      show-battery-percentage = true;
    };
    "org/gnome/desktop/peripherals/touchpad" = {
      two-finger-scrolling-enabled = true;
    };
    "org/gnome/desktop/wm/keybindings" = {
      move-to-monitor-left = "@as []";
      move-to-monitor-right = "@as []";
      move-to-workspace-left = [ "<Shift><Super>Left" ];
      move-to-workspace-right = [ "<Shift><Super>Right" ];
      switch-to-workspace-left = [ "<Control><Super>Left" ];
      switch-to-workspace-right = [ "<Control><Super>Right" ];
      unmaximize = "@as []";
    };

    "org/gnome/desktop/wm/preferences" = {
      button-layout = "appmenu:minimize,maximize,close";
    };
    "org/gnome/mutter" = {
      center-new-windows = true;
      dynamic-workspaces = true;
      edge-tiling = false;
    };

    "org/gnome/settings-daemon/plugins/media-keys" = {
      custom-keybindings = [ "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/" ];
    };
    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
      binding = "<Control><Alt>t";
      command = "kgx";
      name = "terminal";
    };

    "org/gnome/calculator" = {
      accuracy = 12;
      angle-units = "degrees";
      base = 10;
      button-mode = "basic";
      number-format = "automatic";
      refresh-interval = 604800;
      show-thousands = true;
      show-zeroes = false;
      source-currency = "";
      source-units = "degree";
      target-currency = "";
      target-units = "radian";
      word-size = 64;
    };


    "org/gnome/nautilus/icon-view" = {
      captions = [ "date_modified" "size" "none" ];
    };
    "org/gnome/nautilus/list-view" = {
      default-zoom-level = "medium";
      use-tree-view = true;
    };
    "org/gnome/nautilus/preferences" = {
      default-folder-viewer = "list-view";
      migrated-gtk-settings = true;
      search-filter-time-type = "last_modified";
      show-create-link = false;
      show-delete-permanently = false;
    };

    "org/gtk/gtk4/settings/file-chooser" = {
      show-hidden = true;
      sort-directories-first = true;
    };
    "org/gtk/settings/file-chooser" = {
      date-format = "regular";
      location-mode = "path-bar";
      show-hidden = false;
      show-size-column = true;
      show-type-column = true;
      sidebar-width = 157;
      sort-column = "name";
      sort-directories-first = false;
      sort-order = "ascending";
      type-format = "category";
    };

    "org/gnome/shell" = {
      disable-user-extensions = false;
      disabled-extensions = [
        "native-window-placement@gnome-shell-extensions.gcampax.github.com"
        "drive-menu@gnome-shell-extensions.gcampax.github.com"
        "windowsNavigator@gnome-shell-extensions.gcampax.github.com"
        "window-list@gnome-shell-extensions.gcampax.github.com"
      ];
      enabled-extensions = [
        "dash-to-dock@micxgx.gmail.com"
        "workspace-indicator@gnome-shell-extensions.gcampax.github.com"
        "places-menu@gnome-shell-extensions.gcampax.github.com"
        "tiling-assistant@leleat-on-github"
        "apps-menu@gnome-shell-extensions.gcampax.github.com"
        "nightthemeswitcher@romainvigier.fr"
      ];
    };
    "org/gnome/shell/extensions/dash-to-dock" = {
      apply-custom-theme = true;
      background-opacity = 0.80000000000000004;
      click-action = "focus-minimize-or-appspread";
      custom-theme-shrink = true;
      dash-max-icon-size = 32;
      dock-fixed = true;
      dock-position = "LEFT";
      extend-height = true;
      height-fraction = 0.90000000000000002;
      icon-size-fixed = false;
      isolate-workspaces = true;
      multi-monitor = true;
      preferred-monitor = -2;
      preferred-monitor-by-connector = "DP-3";
      preview-size-scale = 1.0;
      show-mounts-network = true;
    };
    "org/gnome/shell/extensions/tiling-assistant" = {
      activate-layout0 = "@as []";
      activate-layout1 = "@as []";
      activate-layout2 = "@as []";
      activate-layout3 = "@as []";
      activate-layout4 = "@as []";
      active-window-hint = 1;
      active-window-hint-color = "rgb(53, 32228)";
      auto-tile = "@as []";
      center-window = "@as []";
      debugging-free-rects = "@as []";
      debugging-show-tiled-rects = "@as []";
      default-move-mode = 0;
      dynamic-keybinding-behavior = 0;
      enable-advanced-experimental-features = true;
      favorite-layouts = [ "0" "-1" "-1" ];
      import-layout-examples = false;
      last-version-installed = 45;
      maximize-with-gap = true;
      restore-window = [ "<Super>Down" ];
      search-popup-layout = [ "<Control><Alt>o" ];
      show-layout-panel-indicator = true;
      tile-bottom-half = "@as []";
      tile-bottom-half-ignore-ta = "@as []";
      tile-bottomleft-quarter = "@as []";
      tile-bottomleft-quarter-ignore-ta = "@as []";
      tile-bottomright-quarter = "@as []";
      tile-bottomright-quarter-ignore-ta = "@as []";
      tile-edit-mode = "@as []";
      tile-left-half = "@as []";
      tile-left-half-ignore-ta = "@as []";
      tile-maximize = "@as []";
      tile-maximize-horizontally = "@as []";
      tile-maximize-vertically = "@as []";
      tile-right-half = "@as []";
      tile-right-half-ignore-ta = "@as []";
      tile-top-half = "@as []";
      tile-top-half-ignore-ta = "@as []";
      tile-topleft-quarter = "@as []";
      tile-topleft-quarter-ignore-ta = "@as []";
      tile-topright-quarter = "@as []";
      tile-topright-quarter-ignore-ta = "@as []";
      tiling-popup-all-workspace = true;
      toggle-always-on-top = "@as []";
      toggle-tiling-popup = "@as []";
    };

    "org/gnome/shell/extensions/nightthemeswitcher/time" = {
      manual-schedule = true;
      nightthemeswitcher-ondemand-keybinding = [ "<Shift> <Super> t" ];
      sunrise = 7.0;
      sunset = 20.0;
    };
  };

  home.packages = with pkgs.gnomeExtensions; [
    dash-to-dock
    tiling-assistant
    night-theme-switcher
  ];
}
