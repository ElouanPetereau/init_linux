{ pkgs, ... }: {
  home.packages = with pkgs; [
    # Multimedia
    obs-studio
    vlc
    spotify
    discord
    # Graphics
    inkscape
    scribus
    gimp
    drawio
    # Internet
    firefox
    chromium
    tor-browser-bundle-bin
    # Office
    libreoffice
    # Version control
    meld
    # Utilities
    pdfarranger
    sqlitebrowser
    wireshark
    virtualbox
    etcher
    usbutils
    libnotify
    veracrypt
    qFlipper
    gparted
    # dev
    vscode
    kicad
    freecad
    fritzing
    universal-ctags
    android-udev-rules
    android-tools
    # languages support
    gcc
    rustup
    texlive.combined.scheme-full
    nixpkgs-fmt
    nixpkgs-lint
    python3Full
  ];
}
