# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      <home-manager/nixos>
    ];

  # Use the systemd-boot EFI boot loader
  boot.loader.systemd-boot.enable = false;
  boot.loader.efi.canTouchEfiVariables = true;

  # grub
  boot.loader.grub = {
    enable = true;
    efiSupport = true;
    enableCryptodisk = true;
    device = "nodev";
  };

  # luks
  boot.initrd.luks.devices = {
    encrypted = {
      device = "dev/disk/by-uuid/7e91847b-5ff0-4e0f-aaed-e0d2c2d3a610";
      preLVM = true;
    };
  };

  # Hostname.
  networking.hostName = "TOASTER";
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Internationalisation properties and time zone.
  time.timeZone = "Europe/Paris";
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    keyMap = "fr";
  };

  # Setup user
  users.users.elouan = {
    isNormalUser = true;
    home = "/home/elouan";
    extraGroups = [ "wheel" "networkmanager" ];
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  # Configure keymap in X11.
  services.xserver.layout = "us,fr";

  # Enable the GNOME Desktop environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # Exclude all useless gnome files from the config.
  environment.gnome.excludePackages = (with pkgs; [
    gnome-photos
    gnome-tour
    gnome-connections
  ]) ++ (with pkgs.gnome; [
    cheese # webcam tool
    gnome-music
    gedit # text editor
    epiphany # web browser
    geary # email
    totem
    gnome-maps
    gnome-weather
    gnome-contacts
    tali # poker game
    iagno # go game
    hitori # sudoku game
    atomix # puzzle game
  ]);

  # Allow to install non-free applications.
  nixpkgs.config = {
    allowUnfree = true;
  };

  # Add system wide packages.
  environment.systemPackages = [
    # git
    pkgs.git
    # gnome tweaks
    pkgs.gnome.gnome-tweaks
    # better less
    pkgs.tailspin
    # better cat
    pkgs.bat
    # better ls
    pkgs.eza
    # better top
    pkgs.bottom
  ];

  # Setup bash shell.
  users.defaultUserShell = pkgs.bash;
  programs.bash.promptInit = ''PS1="[\[\e[31m\]\u\[\e[m\]\[\e[31m\]@\[\e[m\]\[\e[31m\]\h\[\e[m\]]:\[\e[33m\]\w\[\e[m\]#"'';
  programs.bash.enableLsColors = true;
  environment.interactiveShellInit = ''
    alias ls='eza'
    alias less='spin'
    alias cat='bat'
    alias top='btm'
  '';

  # Exclud Xterm
  services.xserver.excludePackages = [ pkgs.xterm ];

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Enable the OpenSSH daemon.
  # services.OpenSSH.enable = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
